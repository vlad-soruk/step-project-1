"use strict"

/*---------- TASK 1 ----------*/

let ourServices = document.querySelectorAll('.our-services-list-item');
let activeItem = null;

// Шукаємо картинку з вкладки
let designPicture = document.querySelectorAll('.web-design-text img');

// Шукаємо текст з вкладки
let designText = document.querySelectorAll('.web-design-text p');

for (let listItem of ourServices) {
    listItem.addEventListener('click', function() {
        if (listItem.classList.contains('active')) {
            return
        }

        if (activeItem) {
            activeItem.classList.remove('active');
        }

        // Прибираємо стилі для вкладки Web Design
        ourServices[0].classList.remove('active'); 
        // Додаємо бордер зліва для вкладки Web Design
        ourServices[0].style.borderLeft = '1px solid rgba(237, 239, 239, 1)';   
        listItem.classList.add('active');
        activeItem = listItem;

        // Текст та картинки для різних вкладок повинні бути різними
        if (activeItem.dataset.tab === 'web') {
            for (let picture of designPicture) {
                if (picture.dataset.tab === 'web') {
                    picture.hidden = false;
                }
                else {
                    picture.hidden = true;
                }
            }
            for (let text of designText) {
                if (text.dataset.tab === 'web') {
                    text.hidden = false;
                }
                else {
                    text.hidden = true;
                }
            }
        }
        else if (activeItem.dataset.tab === 'graphic') {
            for (let picture of designPicture) {
                if (picture.dataset.tab === 'graphic') {
                    picture.hidden = false;
                }
                else {
                    picture.hidden = true;
                }
            }
            for (let text of designText) {
                if (text.dataset.tab === 'graphic') {
                    text.hidden = false;
                }
                else {
                    text.hidden = true;
                }
            }
        }
        else if (activeItem.dataset.tab === 'onlineSupport') {
            for (let picture of designPicture) {
                if (picture.dataset.tab === 'onlineSupport') {
                    picture.hidden = false;
                }
                else {
                    picture.hidden = true;
                }
            }
            for (let text of designText) {
                if (text.dataset.tab === 'onlineSupport') {
                    text.hidden = false;
                }
                else {
                    text.hidden = true;
                }
            }
        }
        else if (activeItem.dataset.tab === 'app') {
            for (let picture of designPicture) {
                if (picture.dataset.tab === 'app') {
                    picture.hidden = false;
                }
                else {
                    picture.hidden = true;
                }
            }
            for (let text of designText) {
                if (text.dataset.tab === 'app') {
                    text.hidden = false;
                }
                else {
                    text.hidden = true;
                }
            }
        }
        else if (activeItem.dataset.tab === 'marketing') {
            for (let picture of designPicture) {
                if (picture.dataset.tab === 'marketing') {
                    picture.hidden = false;
                }
                else {
                    picture.hidden = true;
                }
            }
            for (let text of designText) {
                if (text.dataset.tab === 'marketing') {
                    text.hidden = false;
                }
                else {
                    text.hidden = true;
                }
            }
        }
        else if (activeItem.dataset.tab === 'seoService') {
            for (let picture of designPicture) {
                if (picture.dataset.tab === 'seoService') {
                    picture.hidden = false;
                }
                else {
                    picture.hidden = true;
                }
            }
            for (let text of designText) {
                if (text.dataset.tab === 'seoService') {
                    text.hidden = false;
                }
                else {
                    text.hidden = true;
                }
            }
        }
    })
}

/*---------- TASK 2 ----------*/

// Кнопка Load more у секції Our amazing work імітує 
// завантаження з сервера нових картинок. При її натисканні 
// в секції знизу мають з'явитись ще 12 картинок.
// Після цього кнопка зникає.

// Шукаємо кнопку 'Завантажити' та 'вішаємо' на неї подію
let loadMorePicturesBtn = document.getElementById('load-more-pictures');
console.log(loadMorePicturesBtn);
let amazingWorkGrid = document.querySelector('.amazing-work-grid');
console.log(amazingWorkGrid);

loadMorePicturesBtn.addEventListener('click', function() {

    // Додаємо 12 картинок до грід контейнера
    for (let i = 1; i<=12; i++) {
        let morePictures = document.createElement('div');
        morePictures.classList.add('amazing-work-grid-item');
        let imageSource = document.createElement('img');
        imageSource.src = `step_project_pictures/graphic_design/graphic-design${i}.jpg`;
        morePictures.append(imageSource);

        amazingWorkGrid.append(morePictures);
    }

    // Ховаємо кнопку 'Завантажити'
    loadMorePicturesBtn.hidden = true;
})

/*---------- TASK 3 ----------*/

// Кнопки на вкладці Our amazing work є "фільтрами продукції". 
// Попередньо кожній із картинок потрібно присвоїти одну з чотирьох категорій, 
// Graphic design, Web design, Landing pages, Wordpress.
// При натисканні на кнопку категорії необхідно показати лише ті картинки, 
// які належать до цієї категорії. All показує картинки з усіх категорій. 

// Знаходимо вкладки з хедера секції amazing work
let amazingWorkTabs = document.querySelectorAll('.amazing-work-header ul li');
console.log(amazingWorkTabs);
let activeAmazingWorkTab = amazingWorkTabs[0];

let amazingWorkGridFirstItem = document.querySelector('.amazing-work-grid-item.item-1');

// Відслідковуємо чи вкладка 'All' вже була натиснута чи ні
let tabAllWasClicked = false;

// Перемикання вкладок
amazingWorkTabs.forEach(function(tab){
    tab.addEventListener('click', function() {
        // if (tab.classList.contains('active')) {
        //     return
        // }
        if (activeAmazingWorkTab) {
            activeAmazingWorkTab.classList.remove('active');
        }

        tab.classList.add('active');
        activeAmazingWorkTab = tab;

        if (tab.dataset.tab === 'all' && !tabAllWasClicked) {

            // Записуємо, що вкладка 'Всі' вже була натиснута
            // Це важливо зробити, щоб при повторному натисненні на вкладку
            // кожен раз не завантажувались чергові фото 
            // При натисненні на інші вкладки, будемо записувати в цю змінну
            // false, щоб все правильно працювало
            tabAllWasClicked = true;       

            for (let child of amazingWorkGrid.children) {
                child.hidden = true;
            }
            amazingWorkGridFirstItem.hidden = false;

            for (let i = 1; i<=11; i++) {
                let morePictures = document.createElement('div');
                morePictures.classList.add('amazing-work-grid-item');
                let imageSource = document.createElement('img');
                imageSource.src = `step_project_pictures/amazing_work_example_${i}.png`;
                morePictures.append(imageSource);
        
                amazingWorkGrid.append(morePictures);
            }

            for (let i = 1; i<=12; i++) {
                let morePictures = document.createElement('div');
                morePictures.classList.add('amazing-work-grid-item');
                let imageSource = document.createElement('img');
                imageSource.src = `step_project_pictures/graphic_design/graphic-design${i}.jpg`;
                morePictures.append(imageSource);
        
                amazingWorkGrid.append(morePictures);
            }

             // Ховаємо кнопку 'Завантажити'
             loadMorePicturesBtn.hidden = true;
        }
        
        else if (tab.dataset.tab === 'graphic') {
            tabAllWasClicked = false;

            for (let child of amazingWorkGrid.children) {
                child.hidden = true;
            }
            amazingWorkGridFirstItem.hidden = false;

            for (let pictureSource of IMAGECATEGORY['graphic']) {
                let categoryPictures = document.createElement('div');
                categoryPictures.classList.add('amazing-work-grid-item');

                let image = document.createElement('img');
                image.src = pictureSource;
                categoryPictures.append(image);

                amazingWorkGridFirstItem.after(categoryPictures);
            }

            // Ховаємо кнопку 'Завантажити'
            loadMorePicturesBtn.hidden = true;
        }

        else if (tab.dataset.tab === 'web') {
            tabAllWasClicked = false;

            for (let child of amazingWorkGrid.children) {
                child.hidden = true;
            }
            amazingWorkGridFirstItem.hidden = false;

            for (let pictureSource of IMAGECATEGORY['web']) {
                let categoryPictures = document.createElement('div');
                categoryPictures.classList.add('amazing-work-grid-item');

                let image = document.createElement('img');
                image.src = pictureSource;
                categoryPictures.append(image);

                amazingWorkGridFirstItem.after(categoryPictures);
            }

             // Ховаємо кнопку 'Завантажити'
             loadMorePicturesBtn.hidden = true;
        }

        else if (tab.dataset.tab === 'landing') {
            tabAllWasClicked = false;

            for (let child of amazingWorkGrid.children) {
                child.hidden = true;
            }
            amazingWorkGridFirstItem.hidden = false;

            for (let pictureSource of IMAGECATEGORY['landing']) {
                let categoryPictures = document.createElement('div');
                categoryPictures.classList.add('amazing-work-grid-item');

                let image = document.createElement('img');
                image.src = pictureSource;
                categoryPictures.append(image);

                amazingWorkGridFirstItem.after(categoryPictures);
            }

             // Ховаємо кнопку 'Завантажити'
             loadMorePicturesBtn.hidden = true;
        }

        else if (tab.dataset.tab === 'wordpress') {
            tabAllWasClicked = false;

            for (let child of amazingWorkGrid.children) {
                child.hidden = true;
            }
            amazingWorkGridFirstItem.hidden = false;

            for (let pictureSource of IMAGECATEGORY['wordpress']) {
                let categoryPictures = document.createElement('div');
                categoryPictures.classList.add('amazing-work-grid-item');

                let image = document.createElement('img');
                image.src = pictureSource;
                categoryPictures.append(image);

                amazingWorkGridFirstItem.after(categoryPictures);
            }

             // Ховаємо кнопку 'Завантажити'
             loadMorePicturesBtn.hidden = true;
        }

    })
})

const IMAGECATEGORY = {
    graphic: ['step_project_pictures/graphic_design/graphic-design2.jpg', 'step_project_pictures/graphic_design/graphic-design3.jpg', 'step_project_pictures/graphic_design/graphic-design4.jpg', 'step_project_pictures/graphic_design/graphic-design8.jpg', 'step_project_pictures/graphic_design/graphic-design11.jpg', "step_project_pictures/amazing_work_example_10.png"],
    web: ["step_project_pictures/amazing_work_example_1.png", "step_project_pictures/amazing_work_example_4.png", "step_project_pictures/amazing_work_example_11.png", 'step_project_pictures/graphic_design/graphic-design12.jpg', 'step_project_pictures/graphic_design/graphic-design9.jpg', "step_project_pictures/amazing_work_example_9.png"],
    landing: ["step_project_pictures/amazing_work_example_2.png", "step_project_pictures/amazing_work_example_3.png", 'step_project_pictures/graphic_design/graphic-design5.jpg', 'step_project_pictures/graphic_design/graphic-design10.jpg', "step_project_pictures/amazing_work_example_5.png","step_project_pictures/amazing_work_example_6.png"],
    wordpress: ['step_project_pictures/graphic_design/graphic-design1.jpg', 'step_project_pictures/graphic_design/graphic-design6.jpg', 'step_project_pictures/graphic_design/graphic-design7.jpg', 'step_project_pictures/graphic_design/graphic-design8.jpg', "step_project_pictures/amazing_work_example_7.png"],
}

/*---------- TASK 4 ----------*/

// Карусель на вкладці What people say about theHam має бути робочою, по кліку 
// як на іконку фотографії внизу, так і на стрілки вправо-вліво. У каруселі 
// має змінюватися як картинка, і текст. Карусель обов'язково має бути з анімацією.

// Шукаємо стрілки слайдера та самі фото

let sliderLessSign = document.querySelector('.slider-less-sign');
let sliderMoreSign = document.querySelector('.slider-more-sign');
let photoContainers = document.querySelectorAll('.avatar-wrapper-slider');

// Знаходимо велику аватарку
let bigAvatar = document.querySelector('.photo-of-a-person');

// Знаходимо сам відгук, професію та ім'я людини
let feedback = document.querySelector('.section-feedbacks-text');
let nameOfPeople = document.querySelector('.name-of-people');
let occupationOfPeople = document.querySelector('.occupation-of-people');

// Знаходимо відгуки всіх людей
let peopleFeedbacksText = document.querySelectorAll('.people-feedbacks-hidden-text');
let peopleNamesText = document.querySelectorAll('.people-names-hidden-text');
let peopleOccupationsText = document.querySelectorAll('.people-occupations-hidden-text');
console.log(peopleFeedbacksText);

// Переходи у слайдера повинні бути плавними
// for (let photo of photoContainers) {
//     photo.style.transition = "position 3s ease";
// }

// console.log(photoContainer[2]);

// Початковою точкою на слайдері є Hasan Ali
let num = 2;

// Робимо, щоб аватарка Hasan ALi при зарузці сторінки 
// 'возвишалася' першою, як на макеті
for (let photo of photoContainers) {
   if (photo.children[0].getAttribute('alt') === 'hasan_ali_photo') {
    photo.style.position = 'relative';
    photo.style.top = '-9px';
   }
}

// При клікові, минула аватарка повинна ставати на своє початкове місце
let activeAvatar = photoContainers[2];

sliderLessSign.addEventListener('click', function(){
    num--;
    if (num < 0) {
        num = photoContainers.length - 1;
    }
    if (activeAvatar) {
        activeAvatar.style.position = '';
        activeAvatar.style.top = '';
    }

    if (photoContainers[num].dataset.personName === "Lara") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Lara") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Lara") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Lara") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    else if (photoContainers[num].dataset.personName === "Mark") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Mark") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Mark") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Mark") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    else if (photoContainers[num].dataset.personName === "Alex") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Alex") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Alex") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Alex") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    else if (photoContainers[num].dataset.personName === "Hasan") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Hasan") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Hasan") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Hasan") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    

    photoContainers[num].style.transition = "all 3s ease";
    photoContainers[num].style.position = 'relative';
    photoContainers[num].style.top = '-9px';

    bigAvatar.src = photoContainers[num].children[0].getAttribute('src');

    // Не забуваємо перезаписати activeAvatar
    activeAvatar =  photoContainers[num];
})

sliderMoreSign.addEventListener('click', function(){
    num++;
    if (num >= photoContainers.length) {
        num = 0;
    }
    if (activeAvatar) {
        activeAvatar.style.position = '';
        activeAvatar.style.top = '';
    }

    if (photoContainers[num].dataset.personName === "Lara") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Lara") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Lara") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Lara") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    else if (photoContainers[num].dataset.personName === "Mark") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Mark") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Mark") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Mark") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    else if (photoContainers[num].dataset.personName === "Alex") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Alex") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Alex") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Alex") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    else if (photoContainers[num].dataset.personName === "Hasan") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Hasan") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Hasan") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Hasan") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    

    photoContainers[num].style.position = 'relative';
    photoContainers[num].style.top = '-9px';

    bigAvatar.src = photoContainers[num].children[0].getAttribute('src');

    // Не забуваємо перезаписати activeAvatar
    activeAvatar =  photoContainers[num];
})

for (let avatar of photoContainers) {
    avatar.addEventListener('click', function(){
        if (activeAvatar) {
            activeAvatar.style.position = '';
            activeAvatar.style.top = '';
        }
    
        if (avatar.dataset.personName === "Lara") {
            for (let review of peopleFeedbacksText) {
                if (review.dataset.personName === "Lara") {
                    feedback.textContent = review.textContent;
                }
            }
            for (let name of peopleNamesText) {
                if (name.dataset.personName === "Lara") {
                    nameOfPeople.textContent = name.textContent;
                }
            }
            for (let occupation of peopleOccupationsText) {
                if (occupation.dataset.personName === "Lara") {
                    occupationOfPeople.textContent = occupation.textContent;
                }
            }
        }
        else if (avatar.dataset.personName === "Mark") {
            for (let review of peopleFeedbacksText) {
                if (review.dataset.personName === "Mark") {
                    feedback.textContent = review.textContent;
                }
            }
            for (let name of peopleNamesText) {
                if (name.dataset.personName === "Mark") {
                    nameOfPeople.textContent = name.textContent;
                }
            }
            for (let occupation of peopleOccupationsText) {
                if (occupation.dataset.personName === "Mark") {
                    occupationOfPeople.textContent = occupation.textContent;
                }
            }
        }
        else if (avatar.dataset.personName === "Alex") {
            for (let review of peopleFeedbacksText) {
                if (review.dataset.personName === "Alex") {
                    feedback.textContent = review.textContent;
                }
            }
            for (let name of peopleNamesText) {
                if (name.dataset.personName === "Alex") {
                    nameOfPeople.textContent = name.textContent;
                }
            }
            for (let occupation of peopleOccupationsText) {
                if (occupation.dataset.personName === "Alex") {
                    occupationOfPeople.textContent = occupation.textContent;
                }
            }
        }
        else if (avatar.dataset.personName === "Hasan") {
            for (let review of peopleFeedbacksText) {
                if (review.dataset.personName === "Hasan") {
                    feedback.textContent = review.textContent;
                }
            }
            for (let name of peopleNamesText) {
                if (name.dataset.personName === "Hasan") {
                    nameOfPeople.textContent = name.textContent;
                }
            }
            for (let occupation of peopleOccupationsText) {
                if (occupation.dataset.personName === "Hasan") {
                    occupationOfPeople.textContent = occupation.textContent;
                }
            }
        }
    
        avatar.style.position = 'relative';
        avatar.style.top = '-9px';
    
        bigAvatar.src = avatar.children[0].getAttribute('src');
    
        // Не забуваємо перезаписати activeAvatar
        activeAvatar =  avatar;
    })
}

// Про дублювання коду знаю, буду перероблювати)